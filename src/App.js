import React, {Component} from 'react'
import {BrowserRouter, Route} from 'react-router-dom'
import Hello from './components/hello.jsx';
import World from './components/world.jsx';
import NavBar from './components/navbar.jsx';
import baseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

class App extends Component {
    constructor(props) {
        super(props);
    }

    getChildContext() {
        return {muiTheme: getMuiTheme(baseTheme)};
    }

    render() {
        return (
            <BrowserRouter>
                <div>
                    <NavBar/>
                    <Route exact path='/' component={Hello}/>
                    <Route path='/world' component={World}/>
                </div>
            </BrowserRouter>
        );
    }
}

App.childContextTypes = {
    muiTheme: React.PropTypes.object
};
export default App;