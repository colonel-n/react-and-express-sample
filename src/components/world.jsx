import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import Child from './children/Child'
import {RaisedButton} from 'material-ui';
import {Card, CardHeader} from 'material-ui/Card';

export default class World extends React.Component {
    constructor(props) {
        super(props);
        this.state = {value: ''};

        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }

    handleClick(event) {
        alert(150);
    }

    render() {
        return (
            <Card>
                <CardHeader title={this.props.message} />
                <input type="text" value={this.state.value} onChange={this.handleChange}/>
                <p>{this.state.value}</p>
                <RaisedButton onClick={this.handleClick}>Click Me</RaisedButton>
                <ul>
                    <li><Link to="/">HELLO</Link></li>
                    <li><Link to="/world">WORLD</Link></li>
                </ul>
                <Child message='message from parent'/>
            </Card>
        );
    }
}
// validator
World.propTypes = {
    message: PropTypes.string.isRequired
};
// initial value
World.defaultProps = {message: "hello world!!"};