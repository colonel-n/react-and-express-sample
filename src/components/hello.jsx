import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import styles from '../stylesheets/sample.css';

export default class Hello extends Component {
    render() {
        return (
            <div>
                <div className={styles.greatest}>Hello {this.props.param}</div>
                <ul>
                    <li><Link to="/">HELLO</Link></li>
                    <li><Link to="/world">WORLD</Link></li>
                </ul>
            </div>
        );
    }
}