import React from 'react'

export default class Child extends React.Component {
    render() {
        return (
            <div className='child'>
                <p>Child component writing.</p>
                <p>{this.props.message}</p>
            </div>
        );
    }
}